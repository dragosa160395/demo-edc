global class LeadProcessor  implements Database.Batchable<sObject>{
   global Database.QueryLocator start(Database.BatchableContext bc) {
        // collect the batches of records or objects to be passed to execute
        String query = 'Select id,name, LeadSource From Lead';
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext bc, List<Lead> records){
        // process each batch of records
        List<Lead> leadList = new List<Lead>();
        for(Lead l : records){
            l.LeadSource = 'Dreamforce';
            leadList.add(l);    
        }
        
       update leadList;
    }    
    global void finish(Database.BatchableContext bc){
        // execute any post-processing operations
        if(!Test.isRunningTest())
         Database.executeBatch(new LeadProcessor());
    }    

}