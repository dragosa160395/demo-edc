public with sharing class CampingListController {
	@AuraEnabled
    public static List<Camping_Item__c> getItems(){
        
        return new List<Camping_Item__c>([SELECT Id, Name, Price__c, Quantity__c, Packeed__c FROM Camping_Item__c]);
    }
    
    @AuraEnabled
    public static Camping_Item__c saveItem(Camping_Item__c exp){
        upsert exp;
        return exp;
    }
}