public class CarOverviewController {
    public static List<Car__c> carAvailable {get; set;}
    public String resourceCar {get;set;}    
    public Map<String, String> getResourcesRec {get; set;}
    public String getres {get; set;}
    public String carrr {get;set;}
   public CarOverviewController(){
       getResourcesRec = new Map<String, String>();
       carrr= 'cars/MercedesGTR2.png';
        carAvailable = getCars();
		
        getResources();   
       
    }
    
    public List<Car__c> getCars(){
        return [SELECT Id,Name, Brand__c, Model__c, HP__c, Km__c, Options__c, Description__c, Color__c FROM Car__c ];
    }
    
    public Map<String, String> getResources(){
        for(Car__c c : carAvailable){
            getresourcesRec.put(c.Name,c.Name);
        }
        return getResourcesRec;
    }
}