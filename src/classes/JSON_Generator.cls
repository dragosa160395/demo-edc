public class JSON_Generator {
    public Boolean option {get;set;}
    public Boolean bool {get;set;}
    public Boolean buildchk {get;set;}
    public Boolean rendering {get; set;}
    public String clientName {get;set;}
    public Map<String,String> releaseCycles {get;set;}
    public Map<String,String> consumerMap {get;set;}
    public List<SelectOption> SCMManagers {get;set;}
    public String selectedSCMManager {get;set;}
    public String buildchkConsumerKey {get;set;}
    public String productionConsumerKey {get;set;}
    public String repositoryName {get;set;}
    public String repositoryUrl {get;set;}
    public String myString = '';
    public String myBuildchk = '';
    public String myBuildchkString {get; set;}

    
    public void updateConsumerMap(){
        Set <String> cycleNames = releaseCycles.keySet();
        for(String cycleName : cycleNames){
        	string cycle = releaseCycles.get(cycleName);
            string consumerName = 'Consumer key for ' + cycle;
            if(!consumerMap.containsKey(consumerName)){
            	addConsumerKey(consumerName);
            }
        }
        
    }
       public void change()
    {
        if(option == true)
        { 
            bool = true;
        }
        else
        {
            bool = false;
        }
    }
    
    public void addCycle(){
        Integer mapSize = releaseCycles.keySet().size();
        String cycleName = 'ReleaseCycle ' + (mapSize + 1);
        String releaseCycle = '';
        releasecycles.put(cycleName, releaseCycle);
    }
    
    public void addConsumerKey(String consumerName){
        Integer consumerMapSize = consumerMap.keySet().size();
        String consumerKey = '';
        consumerMap.put(consumerName, consumerKey);
    }
    
    public JSON_Generator()
    {
        String valoare = releaseCycles.get('ReleaseCycle 1');
        String ckey = consumerMap.get('Consumer Key for eee');
        rendering = false;
        releaseCycles = new Map<String,String>();
        //consumerMap = new Map<String,String>();
        //SCMManagers = new List<SelectOption>();
        //SCMManagers.add(new SelectOption('bitbucket-cloud','bitbucket-cloud'));
        //SCMManagers.add(new SelectOption('github','github'));
        //addCycle();
        bool = false;
    }
    
    public void setRender() {
        if(validation()){
        	updateConsumerMap();
        	rendering = true;
        } else {
			ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR,'Fields left missing!'));
        }
    }
    
    public void getJSON() {
        if(validation()){
            myString = createString();
        } else {
			ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR,'Fields left missing!'));
        }
        
    }
    
    public String createString(){
        String firstPart = '{\n ' +
                              '"name": "' + clientName + '", \n' +
                              '"debug": true,\n' +
                              '"inputWaitTime": 300,\n' +
                              '"repository": {\n' +
                                '"type": "GitSCM",\n' +
                                '"scmManager": "' + selectedSCMManager + '",\n' +
                                '"owner": "DROP",\n' +
                                '"name": "' + clientName + '", \n'+
                                '"url": "https://bitbucket.org/deloitte-nl-sfdc/vdl.git",\n' +
                                '"ssh": "git@bitbucket.org:deloitte-nl-sfdc/vdl.git",\n'+
                                '"credentialsId": "dvandelindt-app-password",\n'+
                                '"OAuthCredentialsId": "dvandelindt-bitbucket-oauth"\n' +
                              '},\n';
        String steps = '"steps":{' +
                        '"build":{' +
                          '"modules":[' +
                            '"connectedapps"' +
                         ']' +
                        '}' +
                      '},';
        
        String finalJSON = firstPart + steps;
        return finalJSON;
    }
    
    public String getMyString() {
        return myString;
    }

    public Boolean validation(){
        Boolean valid = true;
        if(String.isEmpty(clientName)){
            valid = false;
        } else {
            for(String cycle : releaseCycles.values()){
                if(String.isEmpty(cycle)){
                    valid = false;
                    break;
                }
            }
            if(String.isEmpty(selectedSCMManager)){
                valid = false;
            }
            if(rendering){
                if(String.isEmpty(productionConsumerKey)){
                    valid = false;
                } else {
                    if(buildchk){
                        if(String.isEmpty(buildchkConsumerKey)){
                            valid = false;
                        }
                    }
                	for(String consumerKey: consumerMap.values()){
                    	if(String.isEmpty(consumerKey)){
                        	valid = false;
                        	break;
                    	}
                	}
                }
            }
        }
        return valid;
    }
}