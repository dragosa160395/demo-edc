public with sharing class CampingItemController {

    @AuraEnabled
    public static Camping_Item__c getCamping(){
        return[Select id,name, Price__c, Quantity__c, Packeed__c From Camping_Item__c limit 1];
    }
}