@isTest
public class AddPrimaryContactTest {
//test conflicts
@testSetup
    static void testSetup(){
        List<Contact> lstcontacts;//comment
        List<Account> insertAccount = new List<Account>();
        for(Integer i=1;i<= 100;i++){
            if(math.mod(i, 2) == 1){
                Account acc = new Account(Name = 'testTrailhead ' + i, BillingState = 'NY');
            insertAccount.add(acc);
            }else{
                                Account acc = new Account(Name = 'testTrailhead ' + i, BillingState = 'CA');
                        insertAccount.add(acc);

            }
        }
        upsert insertAccount; 
        
    }
    
    @isTest
    static void testAddPrimaryContact(){
        Contact c = new Contact(LastName = 'AddPrimaryContactTest');
        insert c;
        List<Account>lstAccount = [Select id, Name from Account limit 100];
       
        
        AddPrimaryContact addP = new AddPrimaryContact(c,'CA');
        
        Test.startTest();        
        System.enqueueJob(addP);
        Test.stopTest(); 
		System.assertEquals(50, [SELECT count() FROM Contact WHERE accountId IN (Select id from Account WHERE BillingState ='CA')]);		        
    }
}