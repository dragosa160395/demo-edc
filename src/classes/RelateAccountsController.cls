public with sharing class RelateAccountsController {
    
    @AuraEnabled
    public static List<Opportunity>getSelectRelatedToOppty(Id currentOpptyId){
        System.debug('IdOpp' + currentOpptyId);
      List<Opportunity> opp = [select id,name,OpportunityRelated__c, OpportunityRelated__r.Name from Opportunity where OpportunityRelated__c =: currentOpptyId];
        System.Debug('oppties' + opp);
        return opp;
    }

}