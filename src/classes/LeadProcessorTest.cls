@isTest
public class LeadProcessorTest {
@testSetup
    static void createData(){
        List<Lead> leadToInsert = new List<Lead>();
        for(Integer i=0;i<=200;i++){
            leadToInsert.add(new Lead(LastName = 'Lead ' + i , Company = 'Deloitte', Status = 'New'));
        }
        insert leadToInsert;
    }
    
    
   @isTest
    static void TestLead(){
        Test.startTest();
        LeadProcessor lp = new LeadProcessor();
        Id batchID = Database.executeBatch(lp);
        Test.stopTest();
    }
}